#!/bin/bash

cat selected_files | grep -v ^# | xargs make CC="/tmp/tcc/bin/tcc -m32 -D__HANDLES=0"

rm bin/blib.a
cat selected_files | grep -v ^# | xargs ar r bin/blib.a 2>/dev/null
ranlib bin/blib.a

# LKRN

# gcc  -DARCH=i386 -DPLATFORM=pcbios -DSECUREBOOT=0 -march=i386 -fomit-frame-pointer -fstrength-reduce -falign-jumps=1 -falign-loops=1 -falign-functions=1 -mpreferred-stack-boundary=2 -mregparm=3 -mrtd -freg-struct-return -m32 -fshort-wchar -Ui386 -Ulinux -DNVALGRIND -Iinclude -I. -Iarch/x86/include -Iarch/i386/include -Iarch/i386/include/pcbios -Os -g -ffreestanding -Wall -W -Wformat-nonliteral  -fno-stack-protector -fno-dwarf2-cfi-asm -fno-exceptions  -fno-unwind-tables -fno-asynchronous-unwind-tables -Wno-address -Wno-stringop-truncation -fno-PIE -no-pie   -Werror -ffunction-sections -fdata-sections -include include/compiler.h -DASM_TCHAR='@' -DASM_TCHAR_OPS='@'   -DOBJECT=version -DBUILD_NAME="\"ipxe.lkrn\"" \
# 	-DVERSION_MAJOR=1 \
# 	-DVERSION_MINOR=0 \
# 	-DVERSION_PATCH=0 \
# 	-DVERSION="\"1.0.0+ (133f)\"" \
# 	-c core/version.c -o bin/version.ipxe.lkrn.o

#   -u obj_pcnet32 --defsym check_obj_pcnet32=obj_pcnet32   -u obj_3c509_eisa --defsym check_obj_3c509_eisa=obj_3c509_eisa   -u obj_thunderx --defsym check_obj_thunderx=obj_thunderx   -u obj_sky2 --defsym check_obj_sky2=obj_sky2   -u obj_sundance --defsym check_obj_sundance=obj_sundance   -u obj_3c90x --defsym check_obj_3c90x=obj_3c90x   -u obj_myson --defsym check_obj_myson=obj_myson   -u obj_myri10ge --defsym check_obj_myri10ge=obj_myri10ge   -u obj_b44 --defsym check_obj_b44=obj_b44   -u obj_cs89x0 --defsym check_obj_cs89x0=obj_cs89x0   -u obj_jme --defsym check_obj_jme=obj_jme   -u obj_w89c840 --defsym check_obj_w89c840=obj_w89c840   -u obj_depca --defsym check_obj_depca=obj_depca   -u obj_natsemi --defsym check_obj_natsemi=obj_natsemi   -u obj_intelx --defsym check_obj_intelx=obj_intelx   -u obj_pnic --defsym check_obj_pnic=obj_pnic   -u obj_icplus --defsym check_obj_icplus=obj_icplus   -u obj_exanic --defsym check_obj_exanic=obj_exanic   -u obj_skeleton --defsym check_obj_skeleton=obj_skeleton   -u obj_dmfe --defsym check_obj_dmfe=obj_dmfe   -u obj_prism2_plx --defsym check_obj_prism2_plx=obj_prism2_plx   -u obj_3c595 --defsym check_obj_3c595=obj_3c595   -u obj_sis900 --defsym check_obj_sis900=obj_sis900   -u obj_bnx2 --defsym check_obj_bnx2=obj_bnx2   -u obj_epic100 --defsym check_obj_epic100=obj_epic100   -u obj_tulip --defsym check_obj_tulip=obj_tulip   -u obj_ne2k_isa --defsym check_obj_ne2k_isa=obj_ne2k_isa   -u obj_intelxvf --defsym check_obj_intelxvf=obj_intelxvf   -u obj_forcedeth --defsym check_obj_forcedeth=obj_forcedeth   -u obj_ena --defsym check_obj_ena=obj_ena   -u obj_tlan --defsym check_obj_tlan=obj_tlan  -u obj_3c529 --defsym check_obj_3c529=obj_3c529   -u obj_velocity --defsym check_obj_velocity=obj_velocity   -u obj_eepro --defsym check_obj_eepro=obj_eepro   -u obj_vmxnet3 --defsym check_obj_vmxnet3=obj_vmxnet3   -u obj_prism2_pci --defsym check_obj_prism2_pci=obj_prism2_pci   -u obj_etherfabric --defsym check_obj_etherfabric=obj_etherfabric   -u obj_ns8390 --defsym check_obj_ns8390=obj_ns8390   -u obj_sis190 --defsym check_obj_sis190=obj_sis190   -u obj_atl1e --defsym check_obj_atl1e=obj_atl1e   -u obj_skge --defsym check_obj_skge=obj_skge   -u obj_realtek --defsym check_obj_realtek=obj_realtek   -u obj_davicom --defsym check_obj_davicom=obj_davicom   -u obj_3c509 --defsym check_obj_3c509=obj_3c509   -u obj_amd8111e --defsym check_obj_amd8111e=obj_amd8111e   -u obj_intel --defsym check_obj_intel=obj_intel   -u obj_3c515 --defsym check_obj_3c515=obj_3c515   -u obj_rhine --defsym check_obj_rhine=obj_rhine   -u obj_intelxl --defsym check_obj_intelxl=obj_intelxl   -u obj_eepro100 --defsym check_obj_eepro100=obj_eepro100   -u obj_smc9000 --defsym check_obj_smc9000=obj_smc9000   -u obj_igbvf_main --defsym check_obj_igbvf_main=obj_igbvf_main   -u obj_phantom --defsym check_obj_phantom=obj_phantom   -u obj_vxge --defsym check_obj_vxge=obj_vxge   -u obj_tg3 --defsym check_obj_tg3=obj_tg3   -u obj_sfc_hunt --defsym check_obj_sfc_hunt=obj_sfc_hunt   -u obj_undi --defsym check_obj_undi=obj_undi   -u obj_rtl8185 --defsym check_obj_rtl8185=obj_rtl8185   -u obj_rtl8180 --defsym check_obj_rtl8180=obj_rtl8180   -u obj_ath5k --defsym check_obj_ath5k=obj_ath5k   -u obj_ath9k --defsym check_obj_ath9k=obj_ath9k   -u obj_linda --defsym check_obj_linda=obj_linda   -u obj_hermon --defsym check_obj_hermon=obj_hermon   -u obj_arbel --defsym check_obj_arbel=obj_arbel   -u obj_qib7322 --defsym check_obj_qib7322=obj_qib7322   -u obj_golan --defsym check_obj_golan=obj_golan   -u obj_hvm --defsym check_obj_hvm=obj_hvm   -u obj_hyperv --defsym check_obj_hyperv=obj_hyperv 

# ld  -m elf_i386 -N --no-check-sections --section-start=.prefix=0   --gc-sections -static -T arch/x86/scripts/pcbios.lds  -u _lkrn_start --defsym check__lkrn_start=_lkrn_start   -u obj_virtio_net --defsym check_obj_virtio_net=obj_virtio_net    -u obj_config --defsym check_obj_config=obj_config   -u obj_config_pcbios --defsym check_obj_config_pcbios=obj_config_pcbios  --defsym pci_vendor_id=0 --defsym pci_device_id=0 -e _lkrn_start bin/version.ipxe.lkrn.o bin/blib.a -o bin/ipxe.lkrn.tmp \
#  --defsym _build_id=`perl -e 'printf "0x%08x", int ( rand ( 0xffffffff ) );'` \
#  --defsym _build_timestamp=1547200427 \
#  -Map bin/ipxe.lkrn.tmp.map 2>&1 | grep "undefined reference to" | sed -e 's|.* undefined reference to '\`'\(.*\)'\''|\1|g'

# HD

# gcc  -DARCH=i386 -DPLATFORM=pcbios -DSECUREBOOT=0 -march=i386 -fomit-frame-pointer -fstrength-reduce -falign-jumps=1 -falign-loops=1 -falign-functions=1 -mpreferred-stack-boundary=2 -mregparm=3 -mrtd -freg-struct-return -m32 -fshort-wchar -Ui386 -Ulinux -DNVALGRIND -Iinclude -I. -Iarch/x86/include -Iarch/i386/include -Iarch/i386/include/pcbios -Os -g -ffreestanding -Wall -W -Wformat-nonliteral  -fno-stack-protector -fno-dwarf2-cfi-asm -fno-exceptions  -fno-unwind-tables -fno-asynchronous-unwind-tables -Wno-address -Wno-stringop-truncation -fno-PIE -no-pie   -Werror -ffunction-sections -fdata-sections -include include/compiler.h -DASM_TCHAR='@' -DASM_TCHAR_OPS='@'   -DOBJECT=version -DBUILD_NAME="\"ipxe.hd\"" \
# 	-DVERSION_MAJOR=1 \
# 	-DVERSION_MINOR=0 \
# 	-DVERSION_PATCH=0 \
# 	-DVERSION="\"1.0.0+ (133f)\"" \
# 	-c core/version.c -o bin/version.ipxe.hd.o

#   -u obj_pcnet32 --defsym check_obj_pcnet32=obj_pcnet32   -u obj_3c509_eisa --defsym check_obj_3c509_eisa=obj_3c509_eisa   -u obj_thunderx --defsym check_obj_thunderx=obj_thunderx   -u obj_sky2 --defsym check_obj_sky2=obj_sky2   -u obj_sundance --defsym check_obj_sundance=obj_sundance   -u obj_3c90x --defsym check_obj_3c90x=obj_3c90x   -u obj_myson --defsym check_obj_myson=obj_myson   -u obj_myri10ge --defsym check_obj_myri10ge=obj_myri10ge   -u obj_b44 --defsym check_obj_b44=obj_b44   -u obj_cs89x0 --defsym check_obj_cs89x0=obj_cs89x0   -u obj_jme --defsym check_obj_jme=obj_jme   -u obj_w89c840 --defsym check_obj_w89c840=obj_w89c840   -u obj_depca --defsym check_obj_depca=obj_depca   -u obj_natsemi --defsym check_obj_natsemi=obj_natsemi   -u obj_intelx --defsym check_obj_intelx=obj_intelx   -u obj_pnic --defsym check_obj_pnic=obj_pnic   -u obj_icplus --defsym check_obj_icplus=obj_icplus   -u obj_exanic --defsym check_obj_exanic=obj_exanic   -u obj_skeleton --defsym check_obj_skeleton=obj_skeleton   -u obj_dmfe --defsym check_obj_dmfe=obj_dmfe   -u obj_prism2_plx --defsym check_obj_prism2_plx=obj_prism2_plx   -u obj_3c595 --defsym check_obj_3c595=obj_3c595   -u obj_sis900 --defsym check_obj_sis900=obj_sis900   -u obj_bnx2 --defsym check_obj_bnx2=obj_bnx2   -u obj_epic100 --defsym check_obj_epic100=obj_epic100   -u obj_tulip --defsym check_obj_tulip=obj_tulip   -u obj_ne2k_isa --defsym check_obj_ne2k_isa=obj_ne2k_isa   -u obj_intelxvf --defsym check_obj_intelxvf=obj_intelxvf   -u obj_forcedeth --defsym check_obj_forcedeth=obj_forcedeth   -u obj_ena --defsym check_obj_ena=obj_ena   -u obj_tlan --defsym check_obj_tlan=obj_tlan   -u obj_virtio_net --defsym check_obj_virtio_net=obj_virtio_net   -u obj_3c529 --defsym check_obj_3c529=obj_3c529   -u obj_velocity --defsym check_obj_velocity=obj_velocity   -u obj_eepro --defsym check_obj_eepro=obj_eepro   -u obj_vmxnet3 --defsym check_obj_vmxnet3=obj_vmxnet3   -u obj_prism2_pci --defsym check_obj_prism2_pci=obj_prism2_pci   -u obj_etherfabric --defsym check_obj_etherfabric=obj_etherfabric   -u obj_ns8390 --defsym check_obj_ns8390=obj_ns8390   -u obj_sis190 --defsym check_obj_sis190=obj_sis190   -u obj_atl1e --defsym check_obj_atl1e=obj_atl1e   -u obj_skge --defsym check_obj_skge=obj_skge   -u obj_realtek --defsym check_obj_realtek=obj_realtek   -u obj_davicom --defsym check_obj_davicom=obj_davicom   -u obj_3c509 --defsym check_obj_3c509=obj_3c509   -u obj_amd8111e --defsym check_obj_amd8111e=obj_amd8111e   -u obj_intel --defsym check_obj_intel=obj_intel   -u obj_3c515 --defsym check_obj_3c515=obj_3c515   -u obj_rhine --defsym check_obj_rhine=obj_rhine   -u obj_intelxl --defsym check_obj_intelxl=obj_intelxl   -u obj_eepro100 --defsym check_obj_eepro100=obj_eepro100   -u obj_smc9000 --defsym check_obj_smc9000=obj_smc9000   -u obj_igbvf_main --defsym check_obj_igbvf_main=obj_igbvf_main   -u obj_phantom --defsym check_obj_phantom=obj_phantom   -u obj_vxge --defsym check_obj_vxge=obj_vxge   -u obj_tg3 --defsym check_obj_tg3=obj_tg3   -u obj_sfc_hunt --defsym check_obj_sfc_hunt=obj_sfc_hunt   -u obj_undi --defsym check_obj_undi=obj_undi   -u obj_rtl8185 --defsym check_obj_rtl8185=obj_rtl8185   -u obj_rtl8180 --defsym check_obj_rtl8180=obj_rtl8180   -u obj_ath5k --defsym check_obj_ath5k=obj_ath5k   -u obj_ath9k --defsym check_obj_ath9k=obj_ath9k   -u obj_linda --defsym check_obj_linda=obj_linda   -u obj_hermon --defsym check_obj_hermon=obj_hermon   -u obj_arbel --defsym check_obj_arbel=obj_arbel   -u obj_qib7322 --defsym check_obj_qib7322=obj_qib7322   -u obj_golan --defsym check_obj_golan=obj_golan   -u obj_hvm --defsym check_obj_hvm=obj_hvm   -u obj_hyperv --defsym check_obj_hyperv=obj_hyperv 

# ld  -m elf_i386 -N --no-check-sections --section-start=.prefix=0   --gc-sections -static -T arch/x86/scripts/pcbios.lds  -u _hd_start --defsym check__hd_start=_hd_start   -u obj_config --defsym check_obj_config=obj_config   -u obj_config_pcbios --defsym check_obj_config_pcbios=obj_config_pcbios  --defsym pci_vendor_id=0 --defsym pci_device_id=0 -e _hd_start bin/version.ipxe.hd.o bin/blib.a -o bin/ipxe.hd.tmp \
# 	--defsym _build_id=`perl -e 'printf "0x%08x", int ( rand ( 0xffffffff ) );'` \
# 	--defsym _build_timestamp=1547203289 \
# 	-Map bin/ipxe.hd.tmp.map

# objdump -ht bin/ipxe.hd.tmp | perl ./util/sortobjdump.pl >> bin/ipxe.hd.tmp.map
# objcopy -O binary -R .zinfo bin/ipxe.hd.tmp bin/ipxe.hd.bin
# objcopy -O binary -j .zinfo bin/ipxe.hd.tmp bin/ipxe.hd.zinfo
# ./util/zbin bin/ipxe.hd.bin bin/ipxe.hd.zinfo > bin/ipxe.hd.zbin
# cp bin/ipxe.hd.zbin bin/ipxe.hd


# DSK

# gcc  -DARCH=i386 -DPLATFORM=pcbios -DSECUREBOOT=0 -march=i386 -fomit-frame-pointer -fstrength-reduce -falign-jumps=1 -falign-loops=1 -falign-functions=1 -mpreferred-stack-boundary=2 -mregparm=3 -mrtd -freg-struct-return -m32 -fshort-wchar -Ui386 -Ulinux -DNVALGRIND -Iinclude -I. -Iarch/x86/include -Iarch/i386/include -Iarch/i386/include/pcbios -Os -g -ffreestanding -Wall -W -Wformat-nonliteral  -fno-stack-protector -fno-dwarf2-cfi-asm -fno-exceptions  -fno-unwind-tables -fno-asynchronous-unwind-tables -Wno-address -Wno-stringop-truncation -fno-PIE -no-pie   -Werror -ffunction-sections -fdata-sections -include include/compiler.h -DASM_TCHAR='@' -DASM_TCHAR_OPS='@'   -DOBJECT=version -DBUILD_NAME="\"ipxe.dsk\"" \
# 	-DVERSION_MAJOR=1 \
# 	-DVERSION_MINOR=0 \
# 	-DVERSION_PATCH=0 \
# 	-DVERSION="\"1.0.0+ (133f)\"" \
# 	-c core/version.c -o bin/version.ipxe.dsk.o

#   -u obj_pcnet32 --defsym check_obj_pcnet32=obj_pcnet32   -u obj_3c509_eisa --defsym check_obj_3c509_eisa=obj_3c509_eisa   -u obj_thunderx --defsym check_obj_thunderx=obj_thunderx   -u obj_sky2 --defsym check_obj_sky2=obj_sky2   -u obj_sundance --defsym check_obj_sundance=obj_sundance   -u obj_3c90x --defsym check_obj_3c90x=obj_3c90x   -u obj_myson --defsym check_obj_myson=obj_myson   -u obj_myri10ge --defsym check_obj_myri10ge=obj_myri10ge   -u obj_b44 --defsym check_obj_b44=obj_b44   -u obj_cs89x0 --defsym check_obj_cs89x0=obj_cs89x0   -u obj_jme --defsym check_obj_jme=obj_jme   -u obj_w89c840 --defsym check_obj_w89c840=obj_w89c840   -u obj_depca --defsym check_obj_depca=obj_depca   -u obj_natsemi --defsym check_obj_natsemi=obj_natsemi   -u obj_intelx --defsym check_obj_intelx=obj_intelx   -u obj_pnic --defsym check_obj_pnic=obj_pnic   -u obj_icplus --defsym check_obj_icplus=obj_icplus   -u obj_exanic --defsym check_obj_exanic=obj_exanic   -u obj_skeleton --defsym check_obj_skeleton=obj_skeleton   -u obj_dmfe --defsym check_obj_dmfe=obj_dmfe   -u obj_prism2_plx --defsym check_obj_prism2_plx=obj_prism2_plx   -u obj_3c595 --defsym check_obj_3c595=obj_3c595   -u obj_sis900 --defsym check_obj_sis900=obj_sis900   -u obj_bnx2 --defsym check_obj_bnx2=obj_bnx2   -u obj_epic100 --defsym check_obj_epic100=obj_epic100   -u obj_tulip --defsym check_obj_tulip=obj_tulip   -u obj_ne2k_isa --defsym check_obj_ne2k_isa=obj_ne2k_isa   -u obj_intelxvf --defsym check_obj_intelxvf=obj_intelxvf   -u obj_forcedeth --defsym check_obj_forcedeth=obj_forcedeth   -u obj_ena --defsym check_obj_ena=obj_ena   -u obj_tlan --defsym check_obj_tlan=obj_tlan   -u obj_3c529 --defsym check_obj_3c529=obj_3c529   -u obj_velocity --defsym check_obj_velocity=obj_velocity   -u obj_eepro --defsym check_obj_eepro=obj_eepro   -u obj_vmxnet3 --defsym check_obj_vmxnet3=obj_vmxnet3   -u obj_prism2_pci --defsym check_obj_prism2_pci=obj_prism2_pci   -u obj_etherfabric --defsym check_obj_etherfabric=obj_etherfabric   -u obj_ns8390 --defsym check_obj_ns8390=obj_ns8390   -u obj_sis190 --defsym check_obj_sis190=obj_sis190   -u obj_atl1e --defsym check_obj_atl1e=obj_atl1e   -u obj_skge --defsym check_obj_skge=obj_skge   -u obj_realtek --defsym check_obj_realtek=obj_realtek   -u obj_davicom --defsym check_obj_davicom=obj_davicom   -u obj_3c509 --defsym check_obj_3c509=obj_3c509   -u obj_amd8111e --defsym check_obj_amd8111e=obj_amd8111e   -u obj_intel --defsym check_obj_intel=obj_intel   -u obj_3c515 --defsym check_obj_3c515=obj_3c515   -u obj_rhine --defsym check_obj_rhine=obj_rhine   -u obj_intelxl --defsym check_obj_intelxl=obj_intelxl   -u obj_eepro100 --defsym check_obj_eepro100=obj_eepro100   -u obj_smc9000 --defsym check_obj_smc9000=obj_smc9000   -u obj_igbvf_main --defsym check_obj_igbvf_main=obj_igbvf_main   -u obj_phantom --defsym check_obj_phantom=obj_phantom   -u obj_vxge --defsym check_obj_vxge=obj_vxge   -u obj_tg3 --defsym check_obj_tg3=obj_tg3   -u obj_sfc_hunt --defsym check_obj_sfc_hunt=obj_sfc_hunt   -u obj_undi --defsym check_obj_undi=obj_undi   -u obj_rtl8185 --defsym check_obj_rtl8185=obj_rtl8185   -u obj_rtl8180 --defsym check_obj_rtl8180=obj_rtl8180   -u obj_ath5k --defsym check_obj_ath5k=obj_ath5k   -u obj_ath9k --defsym check_obj_ath9k=obj_ath9k   -u obj_linda --defsym check_obj_linda=obj_linda   -u obj_hermon --defsym check_obj_hermon=obj_hermon   -u obj_arbel --defsym check_obj_arbel=obj_arbel   -u obj_qib7322 --defsym check_obj_qib7322=obj_qib7322   -u obj_golan --defsym check_obj_golan=obj_golan   -u obj_hvm --defsym check_obj_hvm=obj_hvm   -u obj_hyperv --defsym check_obj_hyperv=obj_hyperv 

# ld  -m elf_i386 -N --no-check-sections --section-start=.prefix=0   --gc-sections -static -T arch/x86/scripts/pcbios.lds  -u _dsk_start --defsym check__dsk_start=_dsk_start   -u obj_virtio_net --defsym check_obj_virtio_net=obj_virtio_net   -u obj_config --defsym check_obj_config=obj_config   -u obj_config_pcbios --defsym check_obj_config_pcbios=obj_config_pcbios  --defsym pci_vendor_id=0 --defsym pci_device_id=0 -e _dsk_start bin/version.ipxe.dsk.o bin/blib.a -o bin/ipxe.dsk.tmp \
# 	--defsym _build_id=`perl -e 'printf "0x%08x", int ( rand ( 0xffffffff ) );'` \
# 	--defsym _build_timestamp=1547204629 \
# 	-Map bin/ipxe.dsk.tmp.map

# objdump -ht bin/ipxe.dsk.tmp | perl ./util/sortobjdump.pl >> bin/ipxe.dsk.tmp.map
# objcopy -O binary -R .zinfo bin/ipxe.dsk.tmp bin/ipxe.dsk.bin
# objcopy -O binary -j .zinfo bin/ipxe.dsk.tmp bin/ipxe.dsk.zinfo
# ./util/zbin bin/ipxe.dsk.bin bin/ipxe.dsk.zinfo > bin/ipxe.dsk.zbin
# cp bin/ipxe.dsk.zbin bin/ipxe.dsk
# perl ./util/padimg.pl --blksize=512 bin/ipxe.dsk


# PERSONAL

#   -u obj_config --defsym check_obj_config=obj_config
#     -u obj_virtio_net --defsym check_obj_virtio_net=obj_virtio_net 
#   -u obj_intel --defsym check_obj_intel=obj_intel
#   bin/version.ipxe.dsk.o

# ld  -m elf_i386 -N --no-check-sections --section-start=.prefix=0   --gc-sections -static -T arch/x86/scripts/pcbios.lds   --defsym pci_vendor_id=0 --defsym pci_device_id=0 -e main bin/blib.a -o bin/ipxe.dsk.tmp \
# 	--defsym _build_id=`perl -e 'printf "0x%08x", int ( rand ( 0xffffffff ) );'` \
# 	--defsym _build_timestamp=1547204629 \
# 	-Map bin/ipxe.dsk.tmp.map #2>&1 | grep "undefined reference to" | sed -e 's|.* undefined reference to '\`'\(.*\)'\''|\1|g'

# objdump -ht bin/ipxe.dsk.tmp | perl ./util/sortobjdump.pl >> bin/ipxe.dsk.tmp.map
# objcopy -O binary -R .zinfo bin/ipxe.dsk.tmp bin/ipxe.dsk.bin
# objcopy -O binary -j .zinfo bin/ipxe.dsk.tmp bin/ipxe.dsk.zinfo
# ./util/zbin bin/ipxe.dsk.bin bin/ipxe.dsk.zinfo > bin/ipxe.dsk.zbin
# cp bin/ipxe.dsk.zbin bin/ipxe.dsk
# perl ./util/padimg.pl --blksize=512 bin/ipxe.dsk

/tmp/tcc/bin/tcc -m32 `cat selected_files | grep -v ^#` /tmp/tcc/lib/tcc/i386-libtcc1.a -o ipxe -vv -nostdlib -nostdinc
