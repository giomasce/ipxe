#ifndef CONFIG_SANBOOT_H
#define CONFIG_SANBOOT_H

/** @file
 *
 * sanboot API configuration
 *
 */

FILE_LICENCE ( GPL2_OR_LATER_OR_UBDL );

#include <config/defaults.h>

#undef SANBOOT_PCBIOS
#define SANBOOT_NULL

#include <config/local/sanboot.h>

#endif /* CONFIG_SANBOOT_H */
