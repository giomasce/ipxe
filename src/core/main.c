/**************************************************************************
iPXE -  Network Bootstrap Program

Literature dealing with the network protocols:
	ARP - RFC826
	RARP - RFC903
	UDP - RFC768
	BOOTP - RFC951, RFC2132 (vendor extensions)
	DHCP - RFC2131, RFC2132 (options)
	TFTP - RFC1350, RFC2347 (options), RFC2348 (blocksize), RFC2349 (tsize)
	RPC - RFC1831, RFC1832 (XDR), RFC1833 (rpcbind/portmapper)

**************************************************************************/

FILE_LICENCE ( GPL2_OR_LATER_OR_UBDL );

#include <ipxe_asmc.h>
#include <stddef.h>
#include <stdio.h>
#include <ipxe/init.h>
#include <ipxe/version.h>
#include <usr/autoboot.h>

const char product_short_name[] = "iPXE";
const char product_version[] = "0.0.asmc";

#include <ipxe/netdevice.h>
#include <usr/ifmgmt.h>
#include <usr/imgmgmt.h>
#include <ipxe/acpipwr.h>

int main_program() {
    struct net_device *netdev;
    int rc;
    struct image *image;

    // Initialize network interfaces
    for_each_netdev(netdev) {
        if ((rc = ifconf(netdev, NULL)) != 0) {
            return rc;
        }
    }

    while (1) {
        char *msg = pop_from_asmc();
        if (msg == NULL) {
            coro_yield();
        } else if (strcmp(msg, "exit") == 0) {
            asmc_free(msg);
            break;
        } else if (strcmp(msg, "download") == 0) {
            downloaded_file *df = asmc_malloc(sizeof(downloaded_file));
            asmc_free(msg);
            msg = pop_from_asmc();
            if ((rc = imgdownload_string(msg, 0, &image))) {
                df->size = 0;
                df->data = NULL;
            } else {
                df->size = image->len;
                df->data = asmc_malloc(image->len);
                memcpy(df->data, (void*) image->data, image->len);
                image_put(image);
            }
            push_to_asmc(df);
            asmc_free(msg);
        } else {
            printf("Unknown command: %s\n", msg);
            asmc_free(msg);
        }
    }

    return 0;
}

/**
 * Main entry point
 *
 * @ret rc		Return status code
 */
__asmcall int main ( void ) {
	int rc;

	/* Perform one-time-only initialisation (e.g. heap) */
	initialise();

	/* Some devices take an unreasonably long time to initialise */
	printf ( "%s initialising devices...", product_short_name );
	startup();
	printf ( "ok\n" );

        rc = main_program();

	/* Attempt to boot */
 /*        if ( ( rc = ipxe ( NULL ) ) != 0 ) */
 /*          goto err_ipxe; */

 /* err_ipxe: */
	shutdown_exit();

        if (1) {
            printf("%s kissing you bye-bye and returning!\n", product_short_name);
        } else {
            printf("%s kissing you bye-bye and shutting down! (return code %d)\n", product_short_name, rc);
            acpi_poweroff();
        }

	return rc;
}
